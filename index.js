const express = require('express');
const app = express();
const AWS = require('aws-sdk');
const bodyParser = require('body-parser')
app.use(bodyParser.json())
AWS.config.update({ region: 'ap-southeast-1' });
// port on which the server listens
const port = 3000;

const docClient = new AWS.DynamoDB.DocumentClient();

const scanAll = async (tableName) => {

    let params = {
        TableName: tableName,
    }
    let scanResults = [];
    let items;
    do {
        items = await docClient.scan(params).promise();
        items.Items.forEach((item) => scanResults.push(item));
        params.ExclusiveStartKey = items.LastEvaluatedKey;
    } while (typeof items.LastEvaluatedKey != "undefined");

    return scanResults;

}

const putItem = async (param) => {
    // let param = {
    //     TableName: tableName,
    //     Item: data
    // }

    return await new Promise((resolve, reject) => {
        docClient.put(param, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(param.Item);
            }
        })
    })
}

const loopPutItem = async (data, tableCopy) => {

    // for (const [item, index] of data) {
    //     let param = {
    //         TableName: tableCopy,
    //         Item: item
    //     }
    //     await putItem(param)

    // }
    console.log("inside loop put item")
    let count = data.length - 1;
    let message = "update finished"
    for (let i = 0; i < data.length; i++) {
        let param = {
            TableName: tableCopy,
            Item: data[i]
        }
        await putItem(param)
        if (i == count) {
            // console.log(`${i} == ${count}`)
            return message
        }
    }

}


app.post("/copyDB", async (req, res) => {

    try {
        // console.log("req -> ", req.body)
        let tableMain = req.body.tableCopy
        // let pmKey = req.primaryKey
        let tableCopy = req.body.tableWantToMove

        console.log(`tableMain : ${tableMain} , tableCopy : ${tableCopy}`)

        let dataScanAll = await scanAll(tableMain)
        console.log("dataScanAll -> ", dataScanAll)

        let response = await loopPutItem(dataScanAll, tableCopy)
        console.log("response -> ", response)
        res.json(200, response);


    } catch (e) {
        console.error(e)
        res.json(400, e);
    }
});

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});